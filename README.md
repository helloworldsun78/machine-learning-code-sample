#Prerequisites

Python 3.4+
pip
virtualenv

#Run

1. copy this project into your local

2. python3 -m venv your-virtualenv-name

3. source your-virtualenv-name/bin/activate

4. cd mistake-in-retro-contest-of-OpenAI/

5. ./requirements.sh (if failed, try chmod a+x requirements.sh). At the last step, require your password.

6. if you want to train the model of Sonic games, you should have their *.md files, after getting the *.md files, execute "python -m retro.import <path to steam folder>". more details(https://github.com/openai/retro)

7. cd src/main/ and python contest_main.py
   the process will start to train the model. If you want to change the game or save the model, you can motify the model/rainbow.py and utils/sonic_util.py

8. If you want to try transfer a pre-training model to a new game, you can copy files of model to src/main/transfer_model/ and motify the path in transfer.py. Finally, execute "python transfer.py"

