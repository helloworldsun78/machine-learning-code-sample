import retro
import sys, argparse

parser=argparse.ArgumentParser()
parser.add_argument('--number', help='the number surffix of playback file')
args = parser.parse_args()
fileSurffix = args.number

fileName = 'SonicTheHedgehog-Genesis-GreenHillZone.Act1-' + fileSurffix + '.bk2'
print("fileName: ", fileName)

movie = retro.Movie(fileName)
movie.step()

env = retro.make(game='SonicTheHedgehog-Genesis', state='GreenHillZone.Act1', use_restricted_actions=retro.Actions.ALL)
env.initial_state = movie.get_state()
env.reset()

while movie.step():
    keys = []
    for m in range(movie.players):
      for i in range(env.num_buttons):
          keys.append(movie.get_key(i, m))

    # print("keys: ", keys)
    _obs, _rew, _done, _info = env.step(keys)
    env.render()

    if _rew != 0:
        print("_rew: ", _rew)
    

input("Press Key to Continue...")