import tensorflow as tf

import logger

try:
    from mpi4py import MPI
except ImportError:
    MPI = None

import sys
sys.path.append('..')

from rollouts import PrioritizedReplayBuffer, NStepPlayer, BatchedPlayer
from envs import BatchedFrameStack, BatchedGymEnv
from utils import AllowBacktracking, make_env
from models import DQN, rainbow_models
from spaces import gym_space_vectorizer

import gym_remote.exceptions as gre

def main():
    # TODO : use bigger memory buffer size (currently 4)

    # TODO : remove the reward of going right or add penalty if go back to visited states
    # the reward function is designed in this way, we are not supposed to change it

    if MPI is None or MPI.COMM_WORLD.Get_rank() == 0:
        rank = 0
        logger.configure()
    else:
        logger.configure(format_strs=[])
        rank = MPI.COMM_WORLD.Get_rank()

    """Run DQN until the environment throws an exception."""
    env = AllowBacktracking(make_env(stack=False, scale_rew=False))
    env = BatchedFrameStack(BatchedGymEnv([[env]]), num_images=4, concat=False)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True # pylint: disable=E1101
    with tf.Session(config=config) as sess:
        dqn = DQN(*rainbow_models(sess,
                                  env.action_space.n,
                                  gym_space_vectorizer(env.observation_space),
                                  min_val=-200,
                                  max_val=200))
        player = NStepPlayer(BatchedPlayer(env, dqn.online_net), 4)
        optim, optimize = dqn.optimize(learning_rate=0.0001)
        sess.run(tf.global_variables_initializer())

        # _num_steps = 3000000
        # _target_interval = 1024
        # _min_buffer_size = 20000
        # _save_iters=1024

        _num_steps = 500000
        _target_interval = 1024
        _min_buffer_size = 20000
        _save_iters=1024

        dqn.train(num_steps=_num_steps, # Make sure an exception arrives before we stop.
                  player=player,
                  replay_buffer=PrioritizedReplayBuffer(500000, 0.5, 0.4, epsilon=0.1),
                  optimize_op=optimize,
                  train_interval=1,
                  target_interval=_target_interval,
                  batch_size=16,
                  min_buffer_size=_min_buffer_size,
                  save_iters=_save_iters)

if __name__ == '__main__':
    try:
        main()
    except gre.GymRemoteError as exc:
        print('exception', exc)
